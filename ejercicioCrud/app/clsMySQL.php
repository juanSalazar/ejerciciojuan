<?php
	class clsMySql
	{
		private $conexion;
		private $servidor = 'localhost';
		private $usuario = 'root';
		private $clave = '';
		private $bd = 'ejercicio';
		public $campo = '';
		public $tabla = '';

		function __construct() { $this->conectar(); }
		function __destruct() { mysqli_close($this->conexion); }

		function conectar()
		{
			$this->conexion = mysqli_connect($this->servidor, $this->usuario, $this->clave, $this->bd) or die ('ERROR '.mysqli_error($this->conexion));
			mysqli_set_charset($this->conexion, 'UTF8');
			return $this->conexion;
		}

		function ejecutar($sql)
		{
			$query = mysqli_query($this->conexion, $sql);
			return ($query) ? true : false;
		}

		function obtenerFilas($sql)
		{
			$query = mysqli_query($this->conexion, $sql);
			return mysqli_num_rows($query);
		}

		function obtenerUltimoId()
		{
			$sql = "SELECT MAX($this->campo) FROM $this->tabla";
			$resultado = mysqli_query($this->conexion, $sql);
			while ($fila = mysqli_fetch_row($resultado)) { return $fila[0]; }
			mysqli_free_result($resultado);
		}

		function consultar($sql)
		{
			$query = mysqli_query($this->conexion, $sql);
			$resultado = array();
			while ($fila = mysqli_fetch_assoc($query)) { $resultado[] = $fila; }
			return $resultado;
		}
	}
?>