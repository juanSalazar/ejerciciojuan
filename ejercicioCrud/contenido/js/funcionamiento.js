$(function()
{
	$(document).on('click', '#registro, #consulta', function() //navegación al hacer click en cada elemento se guarda con la función prop en una variable ruta y luego se carga con load la vista en el secction de contenido.
	{
		var ruta = $(this).prop('id');
		if (ruta=='registro')
		{
			$('#contenido').load('vista/registro.html');
		}
		else
		{
			$('#contenido').load('vista/consulta.html', function()
			{
				var tabla = $('table#alumnos tbody');
				var	filas = '';
				var mensaje = '';
				$.ajax(
				{
					url: 'controladores/controlAlumnos.php', 
					type: 'POST',
					datatype: 'JSON',
					data: {op:2},
					beforeSend: function ()
					{

						$('#mensaje').children().remove();
						$('#mensaje').append('<p>Consultando informacion</p>');
					},
					success: function(data)
					{
						if (data) 
						{
							data = jQuery.parseJSON(data);

							$.each(data, function(i)
							{
							 	filas += '<tr><td>'+ data[i].nombre +'</td><td>'+ data[i].cedula +'</td><td>'+ data[i].mail +'</td><td>'+ data[i].edad +'</td></tr>';
							});

							tabla.append(filas);
						}
						else mensaje= '<p>algo salio mal</p>';
					},
					error: function()
					{
						mensaje='<p>tenemos problemas</p>';
					},
					complete: function()
					{

						$('#mensaje').children().remove();
						$('#mensaje').append(mensaje);
					}
				});
			});
		}
	});
	$(document).on('click','#cargar', function()
	{
		var nombre = $('#nombre').val();
		var cedula = $('#cedula').val();
		var mail = $('#mail').val();
		var edad = $('#edad').val();
		var mensaje = '';
		if (nombre =='' || cedula =='' || mail =='' || edad =='') //validación, muestra un mensaje cuando cualquiera de los campos estan vacios
		{
			alert('Campos vacios por favor llene todos los campos'); 
		}
		else
		{
			$.ajax(
			{
				url: 'controladores/controlAlumnos.php',
				type: 'POST',
				dataType: 'JSON',
				data: {a:nombre, b:cedula, c:mail, d:edad, op:1},
				beforeSend: function()
				{
					$('#mensaje').children().remove();//borra los elementos hijos del elemento con id mensaje
					$('#mensaje').append('<p>PROCESANDO INFORMACIÓN....</p>');
				},
				success: function(data)
				{
					if (data)
					{
						
						mensaje = '<p>REGISTRO COMPLETADO</p>';
					}
					else
					{
						
					    mensaje ='<p>NO SE PUDO COMPLETAR EL REGISTRO INTENTE E NUEVO</p>';
					}
				},
				error: function()
				{
					  mensaje = '<p>A OCURRIDO UN PROBLEMA INTENTE DE NUEVO</p>';
				},
				complete: function()
				{
					$('#mensaje').children().remove();
					$('#mensaje').append(mensaje);
				}
			});
		}
	});
});