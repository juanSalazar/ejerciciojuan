<?php
	class modeloAlumnosProp extends clsMySQL
	{
		function __construct() { parent::__construct(); }
		function __destruct() { parent::__destruct(); }

		public $Id = NULL;
		public $Nombre = NULL;
		public $Cedula = NULL;
		public $Email = NULL;
		public $Edad = NULL;
	}
	class clsAlumnos extends modeloAlumnosProp
	{
		function __construct() { parent::__construct(); }
		function __destruct() { parent::__destruct(); }

		function registrar()
		{
			try
			{
				$sql ="INSERT INTO alumno VALUES(NULL,'$this->Nombre', $this->Cedula, '$this->Email', $this->Edad)";
				return $this->ejecutar($sql);
			}
			catch (Exception $ex) { return $ex.getMessage(); }
		}
		function consultarTodos()
		{
			try
			{
				$sql = "SELECT * FROM alumno ORDER BY id DESC";
				$datos = $this->consultar($sql);
				return (count($datos) > 0) ? $datos : FALSE;
			}
			catch (Exception $ex) { return $ex.getMessage();}
		}
	}
?>