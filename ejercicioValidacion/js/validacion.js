function validarForm() 
{
    var verificar = true;
    var expRegNombre = /^[a-zA-zÑñÁáÉéÍíÓóÚúÜü\s]+$/;
    var expRegEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

    var formulario = document.getElementById("contacto-frm");
    var nombre = document.getElementById("nombre");
    var edad = document.getElementById("edad");
    var email = document.getElementById("email");
    var masculino = document.getElementById("M");
    var femenino = document.getElementById("F");
    var asunto = document.getElementById("asunto");
    var comentarios = document.getElementById("comentarios");

    if(nombre.value == "")//(!nombre.value)
    {
        alert("El campo nombre es requrido");
        nombre.focus();
        verificar = false;
    }
    else if(!expRegNombre.exec(nombre.value))
    {
       alert("El campo nombre solo acepta letras y espacios");
       nombre.focus();
       verificar = false;
    }
    else if(edad.value == "")//(!edad.value)
    {
        alert("El campo edad es requrido");
        edad.focus();
        verificar = false;
    }
    else if (edad.value < 18 || edad.value > 60)
    {
        alert("debes tener un rango de edad entre 18 y 60 años");
        edad.focus();
        verificar = false;
    }
    else if(isNaN(edad.value)) //isNotaNumber
    {
        alert("El campo edad solo acepta numeros");
        edad.focus();
        verificar = false;
    }
    else if(email.value == "")//(!email.value)
    {
        alert("El campo email es requrido");
        email.focus();
        verificar = false;
    }
    else if(!expRegEmail.exec(email.value))
    {
       alert("El campo email no es valido");
       email.focus();
       verificar = false;
    }
    else if (!masculino.checked && !femenino.checked)
    {
      alert("El campo sexo es requerido");
      femenino.focus();
      verificar = false;  
    }
    else if(asunto.value == "")//(!asunto.value)
    {
        alert("El campo asunto es requrido");
        asunto.focus();
        verificar = false;
    }
    else if(comentarios.value == "")//(!comentarios.value)
    {
        alert("El campo comentarios es requrido");
        comentarios.focus();
        verificar = false;
    }
    else if (comentarios.value.length > 255) 
    {
        alert("El campo comentario no puede tener mas de 255 letras");
        comentarios.focus();
        verificar = false;
    }   


    if(verificar == true)//(verificar)
    {
        alert("se ha enviado el formulario");
        // document.contacto-frm.submit();
    }

    // alert("validandio");
    // document.contacto_frm.submit();
}
function limpiarForm() 
{
    alert("limpiando");
    document.getElementById("contacto-frm").reset();
}
window.onload = function()
{
    var botonEnviar, botonLimpiar;
    botonLimpiar = document.getElementById("limpiar");
    botonLimpiar.onclick = limpiarForm;

    botonEnviar = document.contacto_frm.enviar_btn;
    botonEnviar.onclick = validarForm;
}